---
title: Home
---

# Embedded systems / Download

<img src="img/logo_heiafr_color.png">

Welcome to the Download site for the lecture "Embedded Systems" for students of
classes I-2 and T-2 of the "Haute école d'ingénierie et d'architecture de Fribourg".
