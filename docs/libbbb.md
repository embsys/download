---
title: libbbb
chapter: 1
---

# libbbb

Library for the Beaglebone Black used during the lecture "Embedded Systems" 1 and 2.

## Version 1.4.4

- [libbbb-1.4.4-Source.tar.gz](../files/libbbb-1.4.4-Source.tar.gz)
- [libbbb-1.4.4-arm-none-eabi.tar.gz](../files/libbbb-1.4.4-arm-none-eabi.tar.gz)
